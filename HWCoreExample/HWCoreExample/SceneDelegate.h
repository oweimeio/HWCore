//
//  SceneDelegate.h
//  HWCoreExample
//
//  Created by apple on 2022/5/19.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

