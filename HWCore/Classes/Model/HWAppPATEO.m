//
//  HWAppPATEO.m
//  HWCore
//
//  Created by apple on 2020/8/21.
//

#import "HWAppPATEO.h"

@implementation HWAppPATEO

- (BOOL)loginWithInfo:(NSDictionary *)info {
    [super loginWithInfo:info];
    
    HWAppPATEO *app = [HWAppPATEO currentApp];
    app.accessToken = info[@"access-token"];
    app.refreshToken = info[@"refresh-token"];
  
    HWUserPATEO *user = [[HWUserPATEO alloc] init];
    NSDictionary *dic = info[@"userInfo"];
    user.UID = [NSString stringWithFormat:@"%zi", [dic[@"uid"] longValue]];
    //设置uid
    self.UID = user.UID;
    [self persist];
    
    [user updateUserWithInfo:dic];
    
    return [user persist];
}

- (BOOL)logout {
    [super logout];
    
    HWAppPATEO *app = [HWAppPATEO currentApp];
    app.accessToken = nil;
    app.refreshToken = nil;
    //持久化
    return [self persist];
}

+ (HWUserPATEO *)user {
    return [HWUserPATEO userWithUID:[HWAppPATEO currentApp].UID];
}

@end
