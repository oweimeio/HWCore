//
//  HWUser.m
//  HWCore
//
//  Created by 胡伟 on 2019/1/28.
//

#import "HWUser.h"
#import "FCFileManager.h"

@implementation HWUser

+ (instancetype)userWithUID:(NSString *)theUID {
    
    if (theUID == nil || [theUID isKindOfClass:[NSString class]] == NO || [theUID isEqualToString:@""]) {
        NSLog(@"user UID not correct");
        return nil;
    }
    
    NSString *userfolder = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/%@", NSStringFromClass([self class])];
    
    if (![FCFileManager isDirectoryItemAtPath:userfolder]) {
        NSLog(@"user folder not exist");
        
        [FCFileManager createDirectoriesForPath:userfolder];
        return nil;
    }
    
    NSString *userpath = [userfolder stringByAppendingFormat:@"/%@", theUID];
    
    if (![FCFileManager isFileItemAtPath:userpath]) {
        NSLog(@"user file not exist");
        return nil;
    }
    
    return [NSKeyedUnarchiver unarchiveObjectWithFile:userpath];
}

+ (BOOL)removeUserWithUID:(NSString *)theUID {
    if (theUID == nil || [theUID isKindOfClass:[NSString class]] == NO || [theUID isEqualToString:@""]) {
        NSLog(@"user UID not correct");
        return NO;
    }
    
    NSString *userfolder = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/%@", NSStringFromClass([self class])];
    
    if (![FCFileManager isDirectoryItemAtPath:userfolder]) {
        NSLog(@"user folder not exist");
        return YES;
    }
    
    NSString *userpath = [userfolder stringByAppendingFormat:@"/%@", theUID];
    
    if (![FCFileManager isFileItemAtPath:userpath]) {
        NSLog(@"user file not exist");
        return YES;
    }
    
    return [FCFileManager removeItemAtPath:userpath];
}

- (void)updateUserWithInfo:(NSDictionary *)info {
    _name = info[@"username"];
    _gender = [info[@"sex"] integerValue];
    _avatarPath = info[@"photourl"];
    _mobileNum = info[@"tel"];
}

- (void)setValuesForKeysWithDictionary:(NSDictionary<NSString *,id> *)keyedValues {
    
    _name = keyedValues[@"username"];
    _gender = [keyedValues[@"sex"] integerValue];
    _avatarPath = keyedValues[@"photourl"];
    _mobileNum = keyedValues[@"tel"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.name = [aDecoder decodeObjectForKey:@"self.name"];
        self.gender = [aDecoder decodeIntegerForKey:@"self.gender"];
        self.avatarPath = [aDecoder decodeObjectForKey:@"self.avatarPath"];
        self.mobileNum = [aDecoder decodeObjectForKey:@"self.mobileNum"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    
    [aCoder encodeObject:self.name forKey:@"self.name"];
    [aCoder encodeInteger:self.gender forKey:@"self.gender"];
    [aCoder encodeObject:self.avatarPath forKey:@"self.avatarPath"];
    [aCoder encodeObject:self.mobileNum forKey:@"self.mobileNum"];
}

- (id)copyWithZone:(NSZone *)zone {
    HWUser *copy = (HWUser *)[[[self class] allocWithZone:zone] init];
    
    if (copy != nil) {
        copy.name = self.name;
        copy.gender = self.gender;
        copy.avatarPath = self.avatarPath;
        copy.mobileNum = self.mobileNum;
    }
    
    return copy;
}

@end
