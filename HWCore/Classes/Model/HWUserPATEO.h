//
//  HWUserPATEO.h
//  HWCore
//
//  Created by apple on 2020/8/21.
//

#import <Foundation/Foundation.h>
#import "HWUser.h"

NS_ASSUME_NONNULL_BEGIN

@interface HWUserPATEO : HWUser

@property (nonatomic, strong) NSString *aid;
@property (nonatomic, strong) NSString *uid;
@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, strong) NSString *mobilePhone;
@property (nonatomic, strong) NSString *nickName;
@property (nonatomic, strong) NSString *userSeq;

@end

NS_ASSUME_NONNULL_END
