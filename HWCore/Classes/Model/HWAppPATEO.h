//
//  HWAppPATEO.h
//  HWCore
//
//  Created by apple on 2020/8/21.
//

#import "HWApp.h"
#import "HWUserPATEO.h"

NS_ASSUME_NONNULL_BEGIN

@interface HWAppPATEO : HWApp

@property (nonatomic, strong) NSString * __nullable accessToken;
@property (nonatomic, strong) NSString * __nullable refreshToken;

+ (HWUserPATEO *)user;

@end

NS_ASSUME_NONNULL_END
