//
//  HWModel.m
//  HWCore
//
//  Created by apple on 2019/5/22.
//

#import "HWModel.h"
#import "FCFileManager.h"

@implementation HWModel

- (BOOL)persist {
    
    NSString *modeldir = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/%@", NSStringFromClass([self class])];
    
    if (![FCFileManager isDirectoryItemAtPath:modeldir]) {
        // DIRECTORY NOT EXIST
        // THEN
        // CREATE
        [FCFileManager createDirectoriesForPath:modeldir];
    }
    
    if (_UID == nil || [_UID isEqualToString:@""]) {
        NSLog(@"MODEL <%@> UID => nil", NSStringFromClass([self class]));
        return NO;
    }
    
    NSString *modelpath = [modeldir stringByAppendingFormat:@"/%@", _UID];
    
    if ([FCFileManager isFileItemAtPath:modelpath]) {
        // FILE EXIST
        // THEN
        // DELETE
        [FCFileManager removeItemAtPath:modelpath];
    }
    
    // ADD FILE
    BOOL result = [NSKeyedArchiver archiveRootObject:self toFile:modelpath];
    NSLog(@"WRITE(%@) %@\n",
          NSStringFromClass([self class]),
          result ? @"SUCCESS" : @"FAILED");
    return result;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        self.UID = [aDecoder decodeObjectForKey:@"self.UID"];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.UID forKey:@"self.UID"];
}

- (id)copyWithZone:(NSZone *)zone {
    HWModel *copy = (HWModel *)[[[self class] allocWithZone:zone] init];
    
    if (copy != nil) {
        copy.UID = self.UID;
    }
    return copy;
}


@end
