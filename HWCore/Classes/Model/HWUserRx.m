//
//  HWUserRx.m
//  HWCore
//
//  Created by 胡伟 on 2019/1/28.
//

#import "HWUserRx.h"

@implementation HWUserRx

- (void)setValuesForKeysWithDictionary:(NSDictionary<NSString *,id> *)keyedValues {
    [super setValuesForKeysWithDictionary:keyedValues];
    _uid = keyedValues[@"uid"];
    
    _account = keyedValues[@"account"];
    _accountSid = keyedValues[@"accountSId"];
    _password = keyedValues[@"clientPwd"];
    _oaAccount = keyedValues[@"oaAccount"];
    
    _voipAccount = keyedValues[@"voipaccount"];
    _voipPassword = keyedValues[@"voippasswd"];
    
    _companyInvite = keyedValues[@"companyInvite"];
    _companyId = keyedValues[@"companyid"];
    _companyName = keyedValues[@"companyname"];
    
    _deptName = keyedValues[@"depart_name"];
    _duty = keyedValues[@"duty"];
    _isLeader = [keyedValues[@"isLeader"] boolValue];
    _userLevel = keyedValues[@"level"];
    
    _idcardNum = keyedValues[@"idcard"];
    _nickName = keyedValues[@"nickname"];
    _userName = keyedValues[@"username"];
    _staffNum = keyedValues[@"staffNo"];
    _phoneNum = keyedValues[@"phonenum"];
    _avatarUrl = keyedValues[@"photourl"];
    _userInital = keyedValues[@"user_initial"];
    _userPinyin = keyedValues[@"user_pinyin"];
    _onlineState = [keyedValues[@"onlineState"] integerValue];
    
    _authtag = keyedValues[@"authtag"];
    _needModifyPwd = [keyedValues[@"modifypasswd"] boolValue];
}

- (void)updateUserWithInfo:(NSDictionary *)info {
    [super updateUserWithInfo:info];
    
    _uid = info[@"uid"];
    
    _account = info[@"account"];
    _accountSid = info[@"accountSId"];
    _password = info[@"clientPwd"];
    _oaAccount = info[@"oaAccount"];
    
    _voipAccount = info[@"voipaccount"];
    _voipPassword = info[@"voippasswd"];
    
    _companyInvite = info[@"companyInvite"];
    _companyId = info[@"companyid"];
    _companyName = info[@"companyname"];
    
    _deptName = info[@"depart_name"];
    _duty = info[@"duty"];
    _isLeader = [info[@"isLeader"] boolValue];
    _userLevel = info[@"level"];
    
    _idcardNum = info[@"idcard"];
    _nickName = info[@"nickname"];
    _userName = info[@"username"];
    _staffNum = info[@"staffNo"];
    _phoneNum = info[@"phonenum"];
    _avatarUrl = info[@"photourl"];
    _userInital = info[@"user_initial"];
    _userPinyin = info[@"user_pinyin"];
    _onlineState = [info[@"onlineState"] integerValue];
    
    _authtag = info[@"authtag"];
    _needModifyPwd = [info[@"modifypasswd"] boolValue];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.uid = [aDecoder decodeObjectForKey:@"self.uid"];
        self.account = [aDecoder decodeObjectForKey:@"self.account"];
        self.accountSid = [aDecoder decodeObjectForKey:@"self.accountSid"];
        self.password = [aDecoder decodeObjectForKey:@"self.password"];
        self.oaAccount = [aDecoder decodeObjectForKey:@"self.oaAccount"];
        
        self.voipAccount = [aDecoder decodeObjectForKey:@"self.voipAccount"];
        self.voipPassword = [aDecoder decodeObjectForKey:@"self.voipPassword"];
        
        self.companyInvite = [aDecoder decodeObjectForKey:@"self.companyInvite"];
        self.companyId = [aDecoder decodeObjectForKey:@"self.companyId"];
        self.companyName = [aDecoder decodeObjectForKey:@"self.companyName"];
        
        self.deptName = [aDecoder decodeObjectForKey:@"self.deptName"];
        self.duty = [aDecoder decodeObjectForKey:@"self.duty"];
        self.isLeader = [aDecoder decodeBoolForKey:@"self.isLeader"];
        self.userLevel = [aDecoder decodeObjectForKey:@"self.userLevel"];
        
        self.idcardNum = [aDecoder decodeObjectForKey:@"self.idcardNum"];
        self.nickName = [aDecoder decodeObjectForKey:@"self.nickName"];
        self.userName = [aDecoder decodeObjectForKey:@"self.userName"];
        self.staffNum = [aDecoder decodeObjectForKey:@"self.staffNum"];
        self.phoneNum = [aDecoder decodeObjectForKey:@"self.phoneNum"];
        self.avatarUrl = [aDecoder decodeObjectForKey:@"self.avatarUrl"];
        self.userInital = [aDecoder decodeObjectForKey:@"self.userInital"];
        self.userPinyin = [aDecoder decodeObjectForKey:@"self.userPinyin"];
        self.onlineState = [aDecoder decodeIntegerForKey:@"self.onlineState"];
        
        self.authtag = [aDecoder decodeObjectForKey:@"self.authtag"];
        self.needModifyPwd = [aDecoder decodeBoolForKey:@"self.needModifyPwd"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    
    [aCoder encodeObject:self.uid forKey:@"self.uid"];
    [aCoder encodeObject:self.account forKey:@"self.account"];
    [aCoder encodeObject:self.accountSid forKey:@"self.accountSid"];
    [aCoder encodeObject:self.password forKey:@"self.password"];
    [aCoder encodeObject:self.oaAccount forKey:@"self.oaAccount"];
    
    [aCoder encodeObject:self.voipAccount forKey:@"self.voipAccount"];
    [aCoder encodeObject:self.voipPassword forKey:@"self.voipPassword"];

    [aCoder encodeObject:self.companyInvite forKey:@"self.companyInvite"];
    [aCoder encodeObject:self.companyId forKey:@"self.companyId"];
    [aCoder encodeObject:self.companyName forKey:@"self.companyName"];
    
    [aCoder encodeObject:self.deptName forKey:@"self.deptName"];
    [aCoder encodeObject:self.duty forKey:@"self.duty"];
    [aCoder encodeBool:self.isLeader forKey:@"self.isLeader"];
    [aCoder encodeObject:self.userLevel forKey:@"self.userLevel"];
    
    [aCoder encodeObject:self.idcardNum forKey:@"self.idcardNum"];
    [aCoder encodeObject:self.nickName forKey:@"self.nickName"];
    [aCoder encodeObject:self.userName forKey:@"self.userName"];
    [aCoder encodeObject:self.staffNum forKey:@"self.staffNum"];
    [aCoder encodeObject:self.phoneNum forKey:@"self.phoneNum"];
    [aCoder encodeObject:self.avatarUrl forKey:@"self.avatarUrl"];
    [aCoder encodeObject:self.userInital forKey:@"self.userInital"];
    [aCoder encodeObject:self.userPinyin forKey:@"self.userPinyin"];
    [aCoder encodeInteger:self.onlineState forKey:@"self.onlineState"];
    
    [aCoder encodeObject:self.authtag forKey:@"self.authtag"];
    [aCoder encodeBool:self.needModifyPwd forKey:@"self.needModifyPwd"];
}

- (id)copyWithZone:(NSZone *)zone {
    HWUserRx *copy = [[[self class] allocWithZone:zone] init];
    
    if (copy != nil) {
        copy.uid = self.uid;
        copy.account = self.account;
        copy.accountSid = self.accountSid;
        copy.password = self.password;
        copy.oaAccount = self.oaAccount;
        
        copy.voipAccount = self.voipAccount;
        copy.voipPassword = self.voipPassword;
        
        copy.companyInvite = self.companyInvite;
        copy.companyId = self.companyId;
        copy.companyName = self.companyName;
        
        copy.deptName = self.deptName;
        copy.duty = self.duty;
        copy.isLeader = self.isLeader;
        copy.userLevel = self.userLevel;
        
        copy.idcardNum = self.idcardNum;
        copy.userName = self.userName;
        copy.nickName = self.nickName;
        copy.staffNum = self.staffNum;
        copy.phoneNum = self.phoneNum;
        copy.avatarUrl = self.avatarUrl;
        copy.userInital = self.userInital;
        copy.userPinyin = self.userPinyin;
        copy.onlineState = self.onlineState;
        
        copy.authtag = self.authtag;
        copy.needModifyPwd = self.needModifyPwd;
    }
    
    return copy;
}

@end
