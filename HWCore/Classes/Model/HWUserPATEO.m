//
//  HWUserPATEO.m
//  HWCore
//
//  Created by apple on 2020/8/21.
//

#import "HWUserPATEO.h"

@implementation HWUserPATEO

- (void)setValuesForKeysWithDictionary:(NSDictionary<NSString *,id> *)keyedValues {
    
    _aid = keyedValues[@"aid"];
    _uid = keyedValues[@"uid"];
    _avatar = keyedValues[@"avatar"];
    _mobilePhone = keyedValues[@"mobilePhone"];
    _nickName = keyedValues[@"nickName"];
    _userSeq = keyedValues[@"userSeq"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.aid = [aDecoder decodeObjectForKey:@"self.aid"];
        self.uid = [aDecoder decodeObjectForKey:@"self.uid"];
        self.avatar = [aDecoder decodeObjectForKey:@"self.avatar"];
        self.mobilePhone = [aDecoder decodeObjectForKey:@"self.mobilePhone"];
        self.nickName = [aDecoder decodeObjectForKey:@"self.nickName"];
        self.userSeq = [aDecoder decodeObjectForKey:@"self.userSeq"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    
    [aCoder encodeObject:self.aid forKey:@"self.aid"];
    [aCoder encodeObject:self.uid forKey:@"self.uid"];
    [aCoder encodeObject:self.avatar forKey:@"self.avatar"];
    [aCoder encodeObject:self.mobilePhone forKey:@"self.mobilePhone"];
    [aCoder encodeObject:self.nickName forKey:@"self.nickName"];
    [aCoder encodeObject:self.userSeq forKey:@"self.userSeq"];
}

- (id)copyWithZone:(NSZone *)zone {
    HWUserPATEO *copy = (HWUserPATEO *)[[[self class] allocWithZone:zone] init];
    
    if (copy != nil) {
        copy.aid = self.aid;
        copy.uid = self.uid;
        copy.avatar = self.avatar;
        copy.mobilePhone = self.mobilePhone;
        copy.nickName = self.nickName;
        copy.userSeq = self.userSeq;
    }
    
    return copy;
}

- (void)updateUserWithInfo:(NSDictionary *)info {
    [super updateUserWithInfo:info];
    
    _aid = info[@"aid"];
    _uid = info[@"uid"];
    _avatar = info[@"avatar"];
    _mobilePhone = info[@"mobilePhone"];
    _nickName = info[@"nickName"];
    _userSeq = info[@"userSeq"];
}

@end
