//
//  HWModel.h
//  HWCore
//
//  Created by apple on 2019/5/22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HWModel : NSObject <NSCopying, NSCoding>

/**
 模型类 唯一标识符.
 Unique ID.
 */
@property (nonatomic, strong) NSString *UID;

/**
 Persist Model.
 
 持久化模型类
 */
- (BOOL)persist;

@end

NS_ASSUME_NONNULL_END
