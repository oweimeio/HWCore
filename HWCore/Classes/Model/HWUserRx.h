//
//  HWUserRx.h
//  HWCore
//
//  Created by 胡伟 on 2019/1/28.
//

#import <Foundation/Foundation.h>
#import "HWUser.h"

NS_ASSUME_NONNULL_BEGIN

@interface HWUserRx : HWUser

//用户id
@property (nonatomic, strong) NSString *uid;
//账号密码
@property (nonatomic, strong) NSString *account;
@property (nonatomic, strong) NSString *accountSid;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *oaAccount;
//voip
@property (nonatomic, strong) NSString *voipAccount;
@property (nonatomic, strong) NSString *voipPassword;
//企业
@property (nonatomic, strong) NSString *companyInvite;
@property (nonatomic, strong) NSString *companyId;
@property (nonatomic, strong) NSString *companyName;
//部门
@property (nonatomic, strong) NSString *deptName;
@property (nonatomic, strong) NSString *duty;
@property (nonatomic, assign) BOOL isLeader;
@property (nonatomic, strong) NSString *userLevel;//所在层级
//个人信息
@property (nonatomic, strong) NSString *idcardNum;
@property (nonatomic, strong) NSString *nickName;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *staffNum;
@property (nonatomic, strong) NSString *phoneNum;
@property (nonatomic, strong) NSString *avatarUrl;
@property (nonatomic, strong) NSString *userInital;//简写
@property (nonatomic, strong) NSString *userPinyin;//拼音
@property (nonatomic, assign) NSInteger onlineState;//在线状态
//权限
@property (nonatomic, strong) NSString *authtag;
@property (nonatomic, assign) BOOL needModifyPwd; //需要改密码
@end

NS_ASSUME_NONNULL_END
