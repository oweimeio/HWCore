//
//  HWUser.h
//  HWCore
//
//  Created by 胡伟 on 2019/1/28.
//

#import <Foundation/Foundation.h>
#import "HWCoreConst.h"
#import "HWModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HWUser : HWModel

@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong) NSString *avatarPath;

@property (nonatomic, assign) HWUserGender gender;

@property (nonatomic, strong) NSString *mobileNum;

/**
 根据UID获取用户
 
 @param theUID 需要获取用户的UID
 @return 用户实例
 */
+ (instancetype)userWithUID:(NSString *)theUID;

+ (BOOL)removeUserWithUID:(NSString *)theUID;

//跟新app和user信息  仅更新个人信息
- (void)updateUserWithInfo:(NSDictionary *)info;

@end

NS_ASSUME_NONNULL_END
