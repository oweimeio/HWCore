//
//  HWApp.h
//  HWCore
//
//  Created by 胡伟 on 2019/1/28.
//

#import <Foundation/Foundation.h>
#import "HWCoreConst.h"
#import "HWModel.h"
#import "HWUser.h"

@interface HWApp : HWModel

@property (nonatomic, strong) NSString *appId;

@property (nonatomic, strong) NSString *appKey;

@property (nonatomic, strong) NSString *appToken;

@property (nonatomic, assign) BOOL isLoggedIn;

@property (nonatomic, assign) HWAppType appType;

+ (instancetype)currentApp;

+ (HWUser*)user;

//登录
- (BOOL)loginWithInfo:(NSDictionary *)info;
//登出
- (BOOL)logout;

@end

