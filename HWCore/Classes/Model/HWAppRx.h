//
//  HWAppRx.h
//  HWCore
//
//  Created by apple on 2019/5/22.
//

#import "HWApp.h"
#import "HWUserRx.h"

@interface HWAppRx : HWApp

//文件服务器上传地址
@property (nonatomic, strong) NSArray *fileServer;

//文件服务器下载地址
@property (nonatomic, strong) NSArray *lvs;

//服务器接口地址
@property (nonatomic, strong) NSString *restHost;
@property (nonatomic, strong) NSArray *connector;

//sdk配置信息
@property (nonatomic, strong) NSDictionary *sdkConfig;

//音视频地址
@property (nonatomic, strong) NSString *roomId;
@property (nonatomic, strong) NSString *roomUrl;

+ (HWUserRx *)user;

@end
