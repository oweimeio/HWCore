//
//  HWApp.m
//  HWCore
//
//  Created by 胡伟 on 2019/1/28.
//

#import "HWApp.h"

@implementation HWApp

+ (instancetype)currentApp {
    static HWApp *currentApp;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        currentApp = [[[self class] alloc] init];
    });
    return currentApp;
}

+ (HWUser *)user {
    return [HWUser userWithUID:[HWApp currentApp].UID];
}

- (BOOL)loginWithInfo:(NSDictionary *)info {
    HWApp *app = [HWApp currentApp];
    app.UID = info[@"uid"];
    app.appId = info[@"appid"];
    app.appToken = info[@"apptoken"];
    app.appKey = info[@"appKey"];
    app.appType = HWAppTypeiOS;
    app.isLoggedIn = YES;
    
    //持久化
    return [self persist];
}

- (BOOL)logout {
    
    HWApp *app = [HWApp currentApp];
    app.UID = nil;
    app.isLoggedIn = NO;
    app.appId = nil;
    app.appToken = nil;
    app.appKey = nil;
    app.appType = nil;
    
    return [self persist];
}

@end
