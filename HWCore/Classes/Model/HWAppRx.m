//
//  HWAppRx.m
//  HWCore
//
//  Created by apple on 2019/5/22.
//

#import "HWAppRx.h"

NSString *const RXCCPSDKBUNDLE = @"CCPSDKBundle.bundle";
NSString *const RXSDKSERVERADD = @"ServerAddr.xml";

@implementation HWAppRx

- (BOOL)loginWithInfo:(NSDictionary *)info {
    [super loginWithInfo:info];
    
    HWAppRx *app = [HWAppRx currentApp];
    app.fileServer = info[@"fileserver"];
    app.lvs = info[@"lvs"];
    app.restHost = info[@"restHost"];
    app.connector = info[@"connector"];
    app.sdkConfig = info[@"confMap"];
    app.roomId = info[@"roomId"];
    app.roomUrl = info[@"roomUrl"];
    [self persist];
    
    HWUserRx *user = [[HWUserRx alloc] init];
    user.UID = info[@"uid"];
    [user updateUserWithInfo:info];
    
    [self setSdkAddressConfig];
    
    return [user persist];
}

- (BOOL)logout {
    [super logout];
    
    HWAppRx *app = [HWAppRx currentApp];
    app.fileServer = nil;
    app.lvs = nil;
    app.restHost = nil;
    app.connector = nil;
    app.sdkConfig = nil;
    app.roomId = nil;
    app.roomUrl = nil;
    //持久化
    return [self persist];
}

- (void)setSdkAddressConfig {
    NSString *xmlBundle = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:RXCCPSDKBUNDLE];
    NSString *xmlPath = [xmlBundle stringByAppendingPathComponent:RXSDKSERVERADD];
    NSData *xmlData = [NSData dataWithContentsOfFile:xmlPath];
    [self parsedDataFromData:xmlData];
}

- (void)parsedDataFromData:(NSData *)xmlData {
    NSMutableString *document = [[NSMutableString alloc] init];
    if(_connector.count > 0 && _lvs.count > 0 && _fileServer.count > 0) {
        @try {
            NSMutableString *strCon = [[NSMutableString alloc] init];
            for (NSString *strConHost_Port in _connector) {
                NSArray *array = [strConHost_Port componentsSeparatedByString:@":"];
                [strCon appendFormat:@"<server><host>%@</host><port>%@</port></server>",array[0], array[1]];
            }
            NSMutableString *strLVS = [[NSMutableString alloc] init];
            for (NSString *strLVSHost_Port in _lvs) {
                NSArray *array = [strLVSHost_Port componentsSeparatedByString:@":"];
                [strLVS appendFormat:@"<server><host>%@</host><port>%@</port></server>",array[0], array[1]];
            }
            
            NSMutableString *strFS = [[NSMutableString alloc] init];
            for (NSString *strFSHost_Port in _fileServer) {
                NSArray *array = [strFSHost_Port componentsSeparatedByString:@":"];
                [strFS appendFormat:@"<server><host>%@</host><port>%@</port></server>",array[0], array[1]];
            }
            [document appendFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?><ServerAddr version=\"2\"><Connector>%@</Connector><LVS>%@</LVS><FileServer>%@</FileServer></ServerAddr>",
             strCon, strLVS, strFS];
        } @catch (NSException *exception) {
            document = [[NSMutableString alloc] initWithData:xmlData encoding:NSUTF8StringEncoding];
        } @finally {
            
        }
    } else {
        document = [[NSMutableString alloc] initWithData:xmlData encoding:NSUTF8StringEncoding];
    }
    NSString *result = [[NSString alloc] initWithFormat:@"%@",document];
    NSError *error = nil;
    NSString *path = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@",RXCCPSDKBUNDLE, RXSDKSERVERADD]];
    [result writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:&error];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    //服务器配置文件夹
    NSString * config = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"config.data"];
    bool flag = [result writeToFile:config atomically:YES encoding:NSUTF8StringEncoding error:&error];
    if(!flag){
        NSLog(@"Config sdkAddress faild");
    }
}

+ (HWUserRx *)user {
    return [HWUserRx userWithUID:[HWAppRx currentApp].UID];
}

@end
