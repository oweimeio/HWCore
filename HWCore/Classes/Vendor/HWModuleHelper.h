//
//  HWMuduleHelper.h
//  HWCore
//
//  Created by 胡伟 on 2019/5/27.
//

#import <Foundation/Foundation.h>
#import <CTMediator/CTMediator.h>

NS_ASSUME_NONNULL_BEGIN

@interface HWModuleHelper : NSObject

/*
 NSInvocation 方式  可以参多个参数
 */
- (id)runModuleFunc:(NSString *)moduleName funcName:(NSString *)funcName parms:(NSArray *)parms;

/*
 容信的用法 最多只能传2个参数
 */
- (id)rx_runModuleFunc:(NSString *)moduleName funcName:(NSString *)funcName parms:(NSArray *)parms;
- (id)rx_runModuleFunc:(NSString *)moduleName funcName:(NSString *)funcName parms:(NSArray *)parms hasReturn:(BOOL)hasReturn;

/*
 CTMediator 的用法
 */
- (id)ct_performActionWithUrl:(NSURL *)url completion:(void(^)(NSDictionary *info))completion;
// 本地组件调用入口
- (id)ct_performTarget:(NSString *)targetName action:(NSString *)actionName params:(NSDictionary *)params shouldCacheTarget:(BOOL)shouldCacheTarget;

- (void)ct_releaseCachedTargetWithTargetName:(NSString *)targetName;

@end

NS_ASSUME_NONNULL_END
