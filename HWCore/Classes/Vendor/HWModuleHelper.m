//
//  HWMuduleHelper.m
//  HWCore
//
//  Created by 胡伟 on 2019/5/27.
//

#import "HWModuleHelper.h"

@implementation HWModuleHelper

// MARK: - RX
- (id)runModuleFunc:(NSString *)moduleName funcName:(NSString *)funcName parms:(NSArray *)parms {
    
    id target = [self getModule:moduleName];
    SEL selector = NSSelectorFromString(funcName);
    NSMethodSignature *signature = [NSClassFromString(moduleName) instanceMethodSignatureForSelector:selector];
    if (signature == nil) {
        NSLog(@"THERE IS A NULL SIGNATURE");
    }
    // NSInvocation : 利用一个NSInvocation对象包装一次方法调用（方法调用者、方法名、方法参数、方法返回值）
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:signature];
    invocation.target = target;
    invocation.selector = selector;
    
    // 设置参数
    NSInteger paramsCount = signature.numberOfArguments - 2; // 除self、_cmd以外的参数个数
    paramsCount = MIN(paramsCount, parms.count);
    for (NSInteger i = 0; i < paramsCount; i++) {
        id object = parms[i];
        if ([object isKindOfClass:[NSNull class]]) continue;
        [invocation setArgument:&object atIndex:i + 2];
    }
    
    // 调用方法
    [invocation invoke];
    
    // 获取返回值
    id returnValue = nil;
    if (signature.methodReturnLength) { // 有返回值类型，才去获得返回值
        const char *returnType = signature.methodReturnType;
        if (!strcmp(returnType, @encode(id))) {
            [invocation getReturnValue:&returnValue];
        }
        else {
            void *buffer = (void *)malloc(signature.methodReturnLength);
            [invocation getReturnValue:buffer];
            if( !strcmp(returnType, @encode(BOOL)) ) {
                returnValue = [NSNumber numberWithBool:*((BOOL*)buffer)];
            }
            else if( !strcmp(returnType, @encode(NSInteger)) ){
                returnValue = [NSNumber numberWithInteger:*((NSInteger*)buffer)];
            }
            else if( !strcmp(returnType, @encode(int)) ){
                returnValue = [NSNumber numberWithInt:*((int*)buffer)];
            }
            else if( !strcmp(returnType, @encode(NSValue)) ) {
                returnValue = [NSValue valueWithBytes:buffer objCType:returnType];
            }
        }
    }
    
    return returnValue;
}


- (id)rx_runModuleFunc:(NSString *)moduleName funcName:(NSString *)funcName parms:(NSArray *)parms hasReturn:(BOOL)hasReturn{
    id ret = nil;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    id rxModule = [self getModule:moduleName];
    if (rxModule) {
        if ([self moduleIsHaveFunc:rxModule FuncName:funcName]) {
            if (hasReturn) {
                if (parms == nil || parms.count ==0) {
                    ret = [rxModule performSelector:NSSelectorFromString(funcName)];
                } else if (parms.count == 1){
                    ret = [rxModule performSelector:NSSelectorFromString(funcName) withObject:parms[0]];
                } else if ([parms count] == 2){
                    ret = [rxModule performSelector:NSSelectorFromString(funcName) withObject:parms[0] withObject:parms[1]];
                }
            }
            else {
                if (parms == nil || parms.count ==0) {
                    [rxModule performSelector:NSSelectorFromString(funcName)];
                } else if (parms.count == 1){
                    [rxModule performSelector:NSSelectorFromString(funcName) withObject:parms[0]];
                } else if ([parms count] == 2){
                    [rxModule performSelector:NSSelectorFromString(funcName) withObject:parms[0] withObject:parms[1]];
                }
            }
        }
    }
#pragma clang diagnostic pop
    
    return ret;
}

- (id)rx_runModuleFunc:(NSString*)moduleName funcName:(NSString*)funcName parms:(NSArray*)parms {
    return [self rx_runModuleFunc:moduleName funcName:funcName parms:parms hasReturn:NO];
}

- (BOOL)moduleIsHaveFunc:(id)module FuncName:(NSString*)funcName {
    return [module respondsToSelector:NSSelectorFromString(funcName)];
}

- (id)getModule:(NSString *)moduleName{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    if ([NSClassFromString(moduleName) respondsToSelector:NSSelectorFromString(@"sharedInstance")]) {
        return [NSClassFromString(moduleName) performSelector:NSSelectorFromString(@"sharedInstance")];
    } else {
        return [[NSClassFromString(moduleName) alloc]init];
    }
#pragma clang diagnostic pop
}


// MARK: - CTMediator
- (id)ct_performActionWithUrl:(NSURL *)url completion:(void (^)(NSDictionary * _Nonnull))completion {
    return [[CTMediator sharedInstance] performActionWithUrl:url completion:completion];
}

- (id)ct_performTarget:(NSString *)targetName action:(NSString *)actionName params:(NSDictionary *)params shouldCacheTarget:(BOOL)shouldCacheTarget {
    return [[CTMediator sharedInstance] performTarget:targetName action:actionName params:params shouldCacheTarget:shouldCacheTarget];
}

- (void)ct_releaseCachedTargetWithTargetName:(NSString *)targetName {
    return [[CTMediator sharedInstance] releaseCachedTargetWithFullTargetName:targetName];
}

@end
