//
//  HWConstUrl.m
//  HWCore
//
//  Created by 胡伟 on 2019/1/28.
//

#import "HWConstAPI.h"

//账号相关
NSString *const API_RX_PBS_LOGIN = @"/ClientAuth";                              // 注册、登录
NSString *const API_RX_PBS_GETIMAGECODE = @"/common/login/getImgCode";          //验证码
NSString *const API_RX_PBS_GETUSERINFO = @"/GetVOIPUserInfo";                   //个人信息
NSString *const API_RX_PBS_GETSMS = @"/GetAuthSMS";                             //短信

//好友


//企业


//朋友圈
