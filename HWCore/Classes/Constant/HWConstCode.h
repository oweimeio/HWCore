//
//  HWConstCode.h
//  HWCore
//
//  Created by 胡伟 on 2019/1/29.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

extern NSString *const errorDomain;
extern NSString *const statusCode;
extern NSString *const statusMsg;

typedef NS_ENUM(NSUInteger, HWCode) {
    HWCodeSuccess = 0,
    HWCodeErrorFailed = 500,
    HWCodeErrorException = 502,
};

NS_ASSUME_NONNULL_END
