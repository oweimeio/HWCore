//
//  HWConstUrl.h
//  HWCore
//
//  Created by 胡伟 on 2019/1/28.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

//账号相关
extern NSString *const API_RX_PBS_LOGIN;
extern NSString *const API_RX_PBS_GETIMAGECODE;
extern NSString *const API_RX_PBS_GETUSERINFO;
extern NSString *const API_RX_PBS_GETSMS;

NS_ASSUME_NONNULL_END
