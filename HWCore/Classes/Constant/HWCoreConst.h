//
//  HWCoreConst.h
//  HWCore
//
//  Created by 胡伟 on 2019/1/28.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


/**
 app类型

 - HWAppTypeAndroid: 安卓
 - HWAppTypeiOS: ios
 - HWAppTypePC: 电脑客户端
 - HWAppTypeH5: h5
 */
typedef NS_ENUM(NSInteger, HWAppType) {
    HWAppTypeUnkown = 0,
    HWAppTypeAndroid = 1,
    HWAppTypeiOS = 2,
    HWAppTypePC = 3,
    HWAppTypeH5 = 4,
};


/**
 性别

 - UserGenderUnknown: 未知的
 - UserGenderFemale: 女性
 - UserGenderMale: 男性
 */
typedef NS_ENUM(NSInteger, HWUserGender) {
    UserGenderUnknown = -1,
    UserGenderFemale = 0,
    UserGenderMale = 1,
};

/**
 在线状态
 
 - UserLoginStateOffLine: 离线
 - UserLoginStateOnline: 在线
 */
typedef NS_ENUM(NSInteger, UserLoginState) {
    UserLoginStateOffLine = 0,
    UserLoginStateOnline = 1,
};

NS_ASSUME_NONNULL_END
