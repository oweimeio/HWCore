//
//  HWConstCode.m
//  HWCore
//
//  Created by 胡伟 on 2019/1/29.
//

#import "HWConstCode.h"

NSString *const errorDomain = @"com.rx.program.errorDomain";
NSString *const statusCode = @"statusCode";
NSString *const statusMsg = @"statusMsg";
