//
//  HWAlertController.h
//  HWCore
//
//  Created by apple on 2018/9/8.
//

#import <UIKit/UIKit.h>

@interface HWAlertController : UIAlertController

/**
 INITIRAL FUNCTION
 */
+ (instancetype _Nonnull)hw_alertWithDefaultMessage:(NSString * _Nullable)defaultMessage title:(NSString * _Nullable)title message:(NSString * _Nullable)message preferredStyle:(UIAlertControllerStyle)preferredStyle;

+ (instancetype _Nonnull)hw_alertWithTitle:(NSString * _Nullable)title
                               message:(NSString * _Nullable)message
                                 style:(UIAlertControllerStyle)preferredStyle
                     cancelButtonTitle:(NSString * _Nullable)cancelButtonTitle
                     cancelButtonBlock:(void(^_Nullable)(void))cancelBlock
                     otherButtonTitles:(NSArray<NSString *> * _Nullable)otherButtonTitles
                     otherButtonsBlock:(void(^_Nullable)(NSInteger index))otherButtonsBlock;

/**
 SHOW STYLE
 */
- (void)show;

@end

