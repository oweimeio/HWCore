//
//  HWProgressHUD.m
//  HWCore
//
//  Created by apple on 2018/9/20.
//

#import "HWProgressHUD.h"

@implementation HWProgressHUD

+ (void)sv_defaultConfigrations {
    [SVProgressHUD setDefaultStyle: SVProgressHUDStyleLight];
    [SVProgressHUD setDefaultMaskType: SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setMinimumDismissTimeInterval: 1.0];
}

+ (void)sv_show {
    [SVProgressHUD show];
}

+ (void)sv_showWithStatus:(NSString *)status {
    [SVProgressHUD showWithStatus:status];
}

+ (void)sv_showSuccessWithStatus:(NSString *)status {
    [SVProgressHUD showSuccessWithStatus:status];
}

+ (void)sv_showSuccessWithFormatStatus:(NSString *)format, ... {
    [SVProgressHUD showSuccessWithFormatStatus:@"%@", format];
}

+ (void)sv_showErrorWithStatus:(NSString *)status {
    [SVProgressHUD showErrorWithStatus:status];
}

+ (void)sv_showErrorWithFormatStatus:(NSString *)format, ... {
    [SVProgressHUD showErrorWithFormatStatus:@"%@", format];
}

+ (void)sv_showMessage:(NSString *)message withDelay:(NSInteger)seconds {
    [SVProgressHUD showMessage:message withDelay:seconds];
}

+ (void)sv_showProgressHideAfter:(NSInteger)seconds {
    [SVProgressHUD showProgressHideAfter:seconds];
}


+ (void)atm_showWithStatus:(NSString *)status {
    [[self sharedATMView] setBlockTouches:YES];
    [[self sharedATMView] setCaption:status];
    [[self sharedATMView] show];
    [[self sharedATMView] hideAfter:1.5];
}

+ (ATMHud *)sharedATMView {
    static ATMHud *sharedATMView;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedATMView = [[ATMHud alloc] init];
        [[UIApplication sharedApplication].keyWindow addSubview:sharedATMView.view];
    });
    return sharedATMView;
}

@end

@implementation SVProgressHUD (Additions)

+ (void)showSuccessWithFormatStatus:(NSString *)format, ... {
    va_list args;
    id ret;
    
    va_start(args, format);
    if (format == nil) {
        ret = nil;
    } else {
        ret = [[NSString alloc] initWithFormat:format arguments:args];
    }
    va_end(args);
    
    if (ret != nil) {
        [SVProgressHUD showSuccessWithStatus:ret];
    } else {
        [SVProgressHUD showSuccessWithStatus:@""];
    }
}

+ (void)showErrorWithFormatStatus:(NSString *)format, ... {
    
    va_list args;
    id ret;
    
    va_start(args, format);
    if (format == nil) {
        ret = nil;
    } else {
        ret = [[NSString alloc] initWithFormat:format arguments:args];
    }
    va_end(args);
    
    if (ret != nil) {
        [SVProgressHUD showErrorWithStatus:ret];
    } else {
        [SVProgressHUD showErrorWithStatus:@""];
    }
}

// SHOW HUD WHEN IT'S VISIBLE
+ (void)tryToShowErrorWithFormatStatus:(NSString *)format, ... {
    
    va_list args;
    id ret;
    
    va_start(args, format);
    if (format == nil) {
        ret = nil;
    } else {
        ret = [[NSString alloc] initWithFormat:format arguments:args];
    }
    va_end(args);
    
    if (ret != nil && [SVProgressHUD isVisible]) {
        [SVProgressHUD showErrorWithStatus:ret];
    } else if ([SVProgressHUD isVisible]) {
        [SVProgressHUD showErrorWithStatus:@""];
    } else {
        // DO NOTHING
    }
    
}

+ (void)showMessage:(NSString *)message withDelay:(NSInteger)seconds {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(seconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [SVProgressHUD showImage:nil status:message];
    });
}

+ (void)showProgressHideAfter:(NSInteger)seconds {
    [SVProgressHUD show];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(seconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
}

@end
