//
//  HWProgressHUD.h
//  HWCore
//
//  Created by apple on 2018/9/20.
//

#import <Foundation/Foundation.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import <ATMHud/ATMHud.h>

typedef NS_ENUM(NSUInteger, HWProgressHUDStyle) {
    HWProgressHUDStyleSV,  // default
    HWProgressHUDStyleATM,
};

@interface HWProgressHUD : NSObject

@property (nonatomic, assign) HWProgressHUDStyle style;

// YOU CAN SET YOUR FAV STYLE
+ (void)sv_defaultConfigrations;

+ (void)sv_show;

+ (void)sv_showWithStatus:(NSString *)status;

+ (void)sv_showSuccessWithStatus:(NSString *)status;

+ (void)sv_showSuccessWithFormatStatus:(NSString *)format, ... NS_FORMAT_FUNCTION(1, 2);

+ (void)sv_showErrorWithStatus:(NSString *)status;

+ (void)sv_showErrorWithFormatStatus:(NSString *)format, ... NS_FORMAT_FUNCTION(1, 2);

+ (void)sv_showMessage:(NSString *)message withDelay:(NSInteger)seconds;

+ (void)sv_showProgressHideAfter:(NSInteger)seconds;


+ (void)atm_showWithStatus:(NSString *)status;

@end

// MARK: - SVProgressHUD

@interface SVProgressHUD (Additions)

// SHOW SUCCESS STATUS WITH A STRING USING C PRINTG-STYLE FORMATTING
+ (void)showSuccessWithFormatStatus:(NSString *)format, ... NS_FORMAT_FUNCTION(1, 2);

// SHOW ERROR STATUS WITH A STRING USING C PRINTG-STYLE FORMATTING
+ (void)showErrorWithFormatStatus:(NSString *)format, ... NS_FORMAT_FUNCTION(1,2);

// SHOW HUD WHEN IT'S VISIBLE
+ (void)tryToShowErrorWithFormatStatus:(NSString *)format, ... NS_FORMAT_FUNCTION(1,2);

// SHOW HUD THEN HIDE AFTERE DELAY
+ (void)showMessage:(NSString *)message withDelay:(NSInteger)seconds;

// SHOW PROGRESS THEN HIDE AFTERE DELAY
+ (void)showProgressHideAfter:(NSInteger)seconds;

@end
