//
//  HWCoreAPI+PATEO.h
//  AFNetworking
//
//  Created by apple on 2020/8/21.
//
#import "HWCoreAPI.h"

NS_ASSUME_NONNULL_BEGIN

@interface HWCoreAPI (HW)

- (id _Nullable)HW_GET:(NSString * _Nonnull)path parameters:(NSDictionary * _Nonnull)params kCompletionHandler;

- (id _Nullable)HW_POST:(NSString * _Nonnull)path parameters:(NSDictionary * _Nonnull)params kCompletionHandler;

@end

NS_ASSUME_NONNULL_END
