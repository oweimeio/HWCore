//
//  HWCoreAPI+PATEO.m
//  AFNetworking
//
//  Created by apple on 2020/8/21.
//

#import "HWCoreAPI+HW.h"
#import "HWConstCode.h"

@implementation HWCoreAPI (HW)

- (id)HW_GET:(NSString *)path parameters:(NSDictionary *)params completionHandler:(void (^)(id _Nullable, NSError * _Nullable))completionHandler {
    NSString *absolutePath = [self getAbsolutePath:path];
    NSDictionary *param = @{@"Request":
    @{@"head":@{@"reqtime":[self requestTime:[NSDate date]],@"useragent":[self userAgent]},
      @"body":params}};
    return [self GETAbsolutePath:absolutePath parameters:param headers:nil completionHandler:^(id  _Nullable responseObj, NSError * _Nullable error) {
        NSInteger code = [responseObj[@"Response"][@"head"][statusCode] integerValue];
        NSString *msg = responseObj[@"Response"][@"head"][statusMsg];
        if (code == HWCodeSuccess) {
            completionHandler(responseObj[@"Response"][@"body"],nil);
        }
        else {
            NSError *error = [NSError errorWithDomain:errorDomain code:code userInfo:@{NSLocalizedDescriptionKey:msg}];
            completionHandler(nil, error);
        }
    }];
}

- (id)HW_POST:(NSString *)path parameters:(NSDictionary *)params completionHandler:(void (^)(id _Nullable, NSError * _Nullable))completionHandler {
    NSString *absolutePath = [self getAbsolutePath:path];
    NSDictionary *param = @{@"Request":
    @{@"head":@{@"reqtime":[self requestTime:[NSDate date]],@"useragent":[self userAgent]},
      @"body":params}};
    return [self POSTAbsolutePath:absolutePath parameters:param headers:nil completionHandler:^(id  _Nullable responseObj, NSError * _Nullable error) {
        NSInteger code = [responseObj[@"Response"][@"head"][statusCode] integerValue];
        NSString *msg = responseObj[@"Response"][@"head"][statusMsg];
        if (code == HWCodeSuccess) {
            completionHandler(responseObj[@"Response"][@"body"],nil);
        }
        else {
            NSError *error = [NSError errorWithDomain:errorDomain code:code userInfo:@{NSLocalizedDescriptionKey:msg}];
            completionHandler(nil, error);
        }
    }];
}

@end
