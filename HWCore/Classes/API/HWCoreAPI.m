//
//  HWCoreAPI.m
//  HWCore
//
//  Created by apple on 2018/9/13.
//

#import "HWCoreAPI.h"
#import "AFNetworking.h"
#import "AFImageDownloader.h"
#import <CommonCrypto/CommonDigest.h>
#import "FCFileManager.h"

static HWCoreAPI *api = nil;
static AFHTTPSessionManager *manager = nil;
NSString *const LIB_BUNDLE_ID = @"org.cocoapods.HWCore";
NSString *const CONFIG_NAME = @"HWCoreConfig";

@interface HWCoreAPI ()

@property (nonatomic, strong) AFHTTPSessionManager *manager;

@end

@implementation HWCoreAPI

+ (instancetype)sharedAPI {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        api = [[HWCoreAPI alloc] init];
    });
    return api;
}

- (AFHTTPSessionManager *)manager {
    if (!_manager) {
        _manager = [AFHTTPSessionManager manager];
        _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"application/json", @"text/json", @"text/javascript", @"text/plain", nil];
        _manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];;
        _manager.securityPolicy.validatesDomainName = NO;
        _manager.securityPolicy.allowInvalidCertificates = YES;
    };
    [self setupDefaultManagerConfig];
    return _manager;
}

- (void)setupDefaultManagerConfig {
    _manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    AFHTTPRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [requestSerializer setValue:@"application/json;charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    NSString *timeStr = [self requestGMTTime:[NSDate date]];
    [requestSerializer setValue:timeStr forHTTPHeaderField:@"Date"];
    [requestSerializer setValue:[[self md5:[NSString stringWithFormat:@"%@",timeStr]] lowercaseString] forHTTPHeaderField:@"Authorization"];
    requestSerializer.timeoutInterval = 15;
    _manager.requestSerializer = requestSerializer;
}

- (void)setSerializerType:(AFSerializerType)serializerType {
    if (serializerType == AFSerializerTypeHttp) { // 供LN登录使用
        _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
        [requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        requestSerializer.timeoutInterval = 15;
        _manager.requestSerializer = requestSerializer;
    }
}

- (void)startMonitoring:(void (^)(HWNetworkReachabilityStatus, BOOL))block {
    // 1.获得网络监控的管理者
    AFNetworkReachabilityManager *mgr = [AFNetworkReachabilityManager sharedManager];
    // 2.设置网络状态改变后的处理
    [mgr setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        //当前的网络状态
        self->_networkStatus = (HWNetworkReachabilityStatus)status;
        // 当网络状态改变了, 就会调用这个block
        switch (status){
            case AFNetworkReachabilityStatusUnknown: // 未知网络
                self->_isNetworking = YES;
                break;
            case AFNetworkReachabilityStatusNotReachable: // 没有网络(断网)
                self->_isNetworking = NO;
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN: // 手机自带网络
                self->_isNetworking = YES;
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi: // WIFI
                self->_isNetworking = YES;
                break;
        }
        block(self.networkStatus,self.isNetworking);
    }];
    [mgr startMonitoring];
}

- (NSString *)getAbsolutePath:(NSString *)path {
    //BASEURL = SCHEME + DOMAIN + PORT
    NSString *baseUrl = _baseUrl;
    if (!_baseUrl) {
        if ([HWCoreAPI sharedAPI].debug) {
            baseUrl = [self valueForLeaf:@"Network" WithKey:@"baseurl-dev"];
        }
        else {
            baseUrl = [self valueForLeaf:@"Network" WithKey:@"baseurl"];
        }
    }
    
    return [NSString stringWithFormat:@"%@%@", baseUrl, path];
}

- (id)GETAbsolutePath:(NSString *)path parameters:(NSDictionary *)params headers:(nullable NSDictionary<NSString *,NSString *> *)headers completionHandler:(void (^)(id, NSError *))completionHandler {
    return [[HWCoreAPI sharedAPI].manager GET:path parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            completionHandler(responseObject,nil);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            completionHandler(nil,error);
        }];
}

- (id)GET:(NSString *)path parameters:(NSDictionary *)params headers:(nullable NSDictionary<NSString *,NSString *> *)headers completionHandler:(void (^)(id, NSError *))completionHandler {
    NSString *absolutePath = [self getAbsolutePath:path];
    return [self GETAbsolutePath:absolutePath parameters:params headers:headers completionHandler:completionHandler];
}

- (id)POSTAbsolutePath:(NSString *)path parameters:(NSDictionary *)params headers:(nullable NSDictionary<NSString *,NSString *> *)headers completionHandler:(void (^)(id, NSError *))completionHandler {
    return [[HWCoreAPI sharedAPI].manager POST:path parameters:params headers:headers progress:nil
    success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionHandler(responseObject,nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionHandler(nil,error);
    }];
}

- (id)POST:(NSString *)path parameters:(NSDictionary *)params headers:(nullable NSDictionary<NSString *,NSString *> *)headers completionHandler:(void (^)(id, NSError *))completionHandler {
    NSString *absolutePath = [self getAbsolutePath:path];
    return [self POSTAbsolutePath:absolutePath parameters:params headers:headers completionHandler:completionHandler];
}

- (void)DownloadImage:(NSString *)path completionHandler:(void (^)(id, NSError *))completionHandler {
    if (path.length == 0 || [path isEqualToString:@""] || path == nil) {
        return;
    }
    AFImageDownloader *imgd = [AFImageDownloader defaultInstance];
    [imgd downloadImageForURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:path]] success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull responseObject) {
        completionHandler(responseObject,nil);
        NSLog(@"\n\nREQUEST(DOWNLOAD IMAGE) SUCCESS\n\tURL\t%@\n", request.URL.absoluteString);
    } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
        completionHandler(nil,error);
        NSLog(@"\n\nREQUEST(DOWNLOAD IMAGE) FAILED\n\tURL\t%@\n", request.URL.absoluteString);
    }];
}

- (NSURLSessionDataTask *)UploadImage:(UIImage *)image serverPath:(NSString *)serverPath progress:(void (^)(float completed, float total))progress completionHandler:(void (^)(id, NSError *))completionHandler {
    NSString *URLString = serverPath;
//    NSString *filename = @"filename";
    NSDictionary *param = @{@"Request":
                                @{@"head":@{@"reqtime":[self requestTime:[NSDate date]],@"useragent":[self userAgent]},
                                  @"body":UIImageJPEGRepresentation(image, 1)}};
    NSURLSessionDataTask *dataTask = [[HWCoreAPI sharedAPI].manager POST:URLString parameters:param headers:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
//        [formData appendPartWithFileData:UIImageJPEGRepresentation(image, 1) name:@"body" fileName:filename mimeType:@"image/jpeg"];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        progress(uploadProgress.completedUnitCount, uploadProgress.totalUnitCount);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable resp) {
        if ([resp[@"statusCode"] integerValue] == 0) {
            completionHandler(resp[@"data"], nil);
            NSLog(@"\n\nUPLOAD IMAGE(POST) SUCCESS\n\tAPI\t%@\n\tRESULT\t%@\n ", URLString, resp[@"data"]);
        } else {
            completionHandler(nil, resp);
            NSLog(@"\n\nUPLOAD IMAGE(POST) ERROR\n\tAPI\t%@\n\tERROR\tCODE=%@\tMSG=%@\n ", URLString, resp[@"code"], resp[@"msg"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionHandler(nil,error);
        NSLog(@"\n\nUPLOAD IMAGE(POST) FAILED\n\tAPI\t%@\n", URLString);
    }];
    
    return dataTask;
}


// MARK: - CONF

- (NSDictionary *)conf {
    
    NSString *confpath;
    
    confpath = [[NSBundle mainBundle] pathForResource:CONFIG_NAME ofType:@"plist"];
    
    if (confpath == nil || [confpath isEqualToString:@""] == YES || [FCFileManager isFileItemAtPath:confpath] == NO) {
        
        NSLog(@"HWCore WARNING\n\tAPP CONFIGURATION FILE WAS NOT FOUND.\n\t%@", confpath);
        
        // FALLBACK TO LIB DEFAULT
        confpath = [[NSBundle bundleWithIdentifier:LIB_BUNDLE_ID] pathForResource:CONFIG_NAME ofType:@"plist"];
    }
    
    // TRY TO READ APP CONFIGURATION
    NSDictionary *conf = [FCFileManager readFileAtPathAsDictionary:confpath];
    
    if (conf == nil) {
        NSLog(@"HWCore ERROR\n\tCONFIGURATION FILE WAS NEVER FOUND.");
    }
    
    return conf;
}

- (id)valueForLeaf:(NSString *)leafName WithKey:(NSString *)key {
    // IF
    if (leafName == nil || [leafName isEqualToString:@""] || [leafName isKindOfClass:[NSString class]] == NO) {
        return nil;
    }
    
    if (key == nil || [key isEqualToString:@""] || [key isKindOfClass:[NSString class]] == NO) {
        return [self conf][leafName];
    }
    
    // GET VALUE
    if ([self conf][leafName][key]) {
        return [self conf][leafName][key];
    } else {
        return nil;
    }
}

- (NSString *)userAgent {
    if (!_userAgent) {
        int width = [[UIScreen mainScreen] bounds].size.width*[[UIScreen mainScreen] scale];
        int hight = [[UIScreen mainScreen] bounds].size.height*[[UIScreen mainScreen] scale];
        NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
        _userAgent = [NSString stringWithFormat:@"%@;%@;%@;%d*%d;%@", [[UIDevice currentDevice] name], [[UIDevice currentDevice] model], [[UIDevice currentDevice] systemVersion], width, hight, version];
    }
    return _userAgent;
}

- (NSString *)requestTime:(NSDate*)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    
    return [dateFormatter stringFromDate:date];
}

- (NSString *)requestGMTTime:(NSDate*)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    dateFormatter.dateFormat = @"EEE MMM dd HH:mm:ss yyyy";
    return [dateFormatter stringFromDate:date];
}

- (NSString *)md5:(NSString *)str {
    NSData *utfStr = [str dataUsingEncoding:NSUTF8StringEncoding];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(utfStr.bytes, (CC_LONG)[utfStr length], result);
    
    return [NSString stringWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X", result[0], result[1], result[2], result[3], result[4], result[5], result[6], result[7], result[8], result[9], result[10], result[11], result[12], result[13], result[14], result[15]];
}

@end
