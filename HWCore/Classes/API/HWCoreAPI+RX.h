//
//  HWCoreAPI+RX.h
//  HWCore
//
//  Created by 胡伟 on 2019/1/28.
//

#import "HWCoreAPI.h"

NS_ASSUME_NONNULL_BEGIN

@interface HWCoreAPI (RX)

/**
 容信上传图片的方法
 
 @param image 图片
 @param params appId:必选参数   userName:选填
 @param completion 回调函数
 @return 返回一个task
 */
- (NSURLSessionUploadTask * _Nonnull)rx_uploadTaskWithImage:(UIImage * _Nonnull)image param:(NSDictionary * _Nonnull)params progress:(void (^_Nullable)(float completed, float total))progress completion:(void (^_Nullable)(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error))completion;

//批量上传图片
- (void)rx_uploadImages:(NSArray * _Nonnull)images param:(NSDictionary *_Nonnull)params progress:(void (^_Nullable)(float completed, float total))progress completion:(void (^_Nonnull)(NSArray *_Nonnull infos))completion;

- (NSURLSessionUploadTask *_Nonnull)rx_uploadTaskWithFileName:(NSString *_Nonnull)fileName fileData:(NSData *_Nonnull)data param:(NSDictionary *_Nonnull)params progress:(void (^_Nullable)(float completed, float total))progress completion:(void (^_Nullable)(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error))completion;


/**
 获取验证码

 @param completionHandler 返回image
 */
- (void)getVerificationCode:(void(^)(UIImage *image, NSString *codeKey, NSError *error))completionHandler;

@end

NS_ASSUME_NONNULL_END
