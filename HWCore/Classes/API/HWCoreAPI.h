//
//  HWCoreAPI.h
//  HWCore
//
//  Created by apple on 2018/9/13.
//

#import <Foundation/Foundation.h>

#define kCompletionHandler completionHandler:(void(^ _Nullable)(id _Nullable responseObj, NSError * _Nullable error))completionHandler

typedef NS_ENUM(NSInteger, HWNetworkReachabilityStatus) {
    HWNetworkReachabilityStatusUnknown          = -1,
    HWNetworkReachabilityStatusNotReachable     = 0,
    HWNetworkReachabilityStatusReachableViaWWAN = 1,
    HWNetworkReachabilityStatusReachableViaWiFi = 2,
};

typedef NS_ENUM(NSInteger, AFSerializerType) {
    AFSerializerTypeJSon            = 0,
    AFSerializerTypeHttp            = 1,
};

@interface HWCoreAPI : NSObject

/// 调试模式用于选择域名等信息
@property (assign, nonatomic) BOOL debug;
/// 设置AFSerializerType
@property (assign, nonatomic) AFSerializerType serializerType;

/// 设置BASEURL = SCHEME + DOMAIN + PORT
/// 如果没设置可以在HWCoreConfig.plist中设置
@property (strong, nonatomic) NSString * _Nullable baseUrl;

@property (strong, nonatomic) NSString * _Nullable userAgent;

/// 当前网络状态
@property (assign, nonatomic, readonly) HWNetworkReachabilityStatus networkStatus;

/// 当前是否有网络
@property (assign, nonatomic, readonly) BOOL isNetworking;

///单例
+ (instancetype _Nonnull)sharedAPI;

/// 监测网络
- (void)startMonitoring:(void(^_Nullable)(HWNetworkReachabilityStatus networkStatus, BOOL isNetworking))block;

- (id _Nullable)GET:(NSString * _Nonnull)path parameters:(NSDictionary * _Nullable)params headers:(nullable NSDictionary<NSString *,NSString *> *)headers kCompletionHandler;

- (id _Nullable)GETAbsolutePath:(NSString * _Nonnull)path parameters:(NSDictionary * _Nullable)params headers:(nullable NSDictionary<NSString *,NSString *> *)headers kCompletionHandler;

- (id _Nullable)POST:(NSString * _Nonnull)path parameters:(NSDictionary * _Nullable)params headers:(nullable NSDictionary<NSString *,NSString *> *)headers kCompletionHandler;

- (id _Nullable)POSTAbsolutePath:(NSString * _Nonnull)path parameters:(NSDictionary * _Nullable)params headers:(nullable NSDictionary<NSString *,NSString *> *)headers kCompletionHandler;

- (void)DownloadImage:(NSString * _Nonnull)path kCompletionHandler;


// 下面方法供分类或子类使用
- (void)setupDefaultManagerConfig;

- (id _Nullable)valueForLeaf:(NSString * _Nonnull)leafName WithKey:(NSString * _Nonnull)key;
/// 获取绝对路径
- (NSString * _Nonnull)getAbsolutePath:(NSString * _Nonnull)path;

- (NSString * _Nullable)requestTime:(NSDate* _Nonnull)date;

- (NSString * _Nullable)requestGMTTime:(NSDate* _Nonnull)date;

- (NSString * _Nullable)md5:(NSString * _Nonnull)str;

@end
