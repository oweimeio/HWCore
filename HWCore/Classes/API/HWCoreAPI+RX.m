//
//  HWCoreAPI+RX.m
//  HWCore
//
//  Created by 胡伟 on 2019/1/28.
//

#import "HWCoreAPI+RX.h"
#import "HWConstAPI.h"
#import "AFNetworking.h"
#import "HWCoreAPI+HW.h"

@implementation HWCoreAPI (RX)


// MARK: - Only userful for Rongxin_Standard
- (NSURLSessionUploadTask *)rx_uploadTaskWithImage:(UIImage *)image param:(NSDictionary *)params progress:(void (^)(float completed, float total))progress completion:(void (^)(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error))completion {
    NSString *fileUrl = [self valueForLeaf:@"Network" WithKey:@"fileserver"] ?: @"http://139.199.128.158:8090";
    NSString *fileName = [NSString stringWithFormat:@"%@.jpg",[self requestTime:[NSDate date]]];
    NSData *fileData = UIImageJPEGRepresentation(image, 1);
    return [self rx_uploadTaskWithFileName:fileName fileData:fileData param:params progress:progress completion:completion];
}

- (void)rx_uploadImages:(NSArray *)images param:(NSDictionary *)params progress:(void (^)(float completed, float total))progress completion:(void (^)(NSArray *infos))completion {
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:images.count];
    for (UIImage *image in images) {
        [result addObject:[NSNull null]];
    }
    dispatch_group_t group = dispatch_group_create();
    for (NSInteger i = 0; i < images.count; i++) {
        dispatch_group_enter(group);
        NSURLSessionUploadTask *uploadTask = [self rx_uploadTaskWithImage:images[i] param:params progress:^(float completed, float total) {
            progress(completed, total);
        } completion:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
            if (error) {
                dispatch_group_leave(group);
            }
            else {
                @synchronized (result) {
                    result[i] = responseObject;
                }
                dispatch_group_leave(group);
            }
        }];
        [uploadTask resume];
    }
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        completion(result);
    });
}

- (NSURLSessionUploadTask *)rx_uploadTaskWithFileName:(NSString *)fileName fileData:(NSData *)data param:(NSDictionary *)params progress:(void (^)(float completed, float total))progress completion:(void (^)(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error))completion {
    NSString *fileUrl = [self valueForLeaf:@"Network" WithKey:@"fileserver"] ?: @"http://139.199.128.158:8090";
    NSAssert(params[@"appId"], @"params have to contains appid\n");
    NSString *appId = params[@"appId"];
    NSString *userName = params[@"userName"] ?: @"tempUser";
    NSString *sig = [self md5:@"yuntongxunytx123"];
    NSString *URLString = [NSString stringWithFormat:@"%@/2015-03-26/Corp/yuntongxun/Upload/VTM?appId=%@2&userName=%@&fileName=%@&sig=%@",fileUrl,appId,userName,fileName,sig];
    NSMutableURLRequest *requestM = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URLString] cachePolicy:0 timeoutInterval:15];
    requestM.HTTPMethod = @"POST";
    requestM.HTTPBody = data;
    [requestM setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSURLSessionUploadTask *uploadTask = [manager uploadTaskWithStreamedRequest:requestM progress:^(NSProgress * _Nonnull uploadProgress) {
        progress(uploadProgress.completedUnitCount, uploadProgress.totalUnitCount);
    } completionHandler:completion];
    return uploadTask;
}

- (void)getVerificationCode:(void (^)(UIImage *image, NSString *codeKey, NSError *error))completionHandler {
    NSString *codeKey = [[NSUUID UUID] UUIDString]; //每次都会变
    [[HWCoreAPI sharedAPI] HW_POST:API_RX_PBS_GETIMAGECODE parameters:@{@"codeKey": codeKey} completionHandler:^(id responseObj, NSError *error) {
        if (error) {
            completionHandler(nil, codeKey, error);
        }
        else {
            UIImage *image = [UIImage imageWithData:[[NSData alloc] initWithBase64EncodedString:responseObj[@"imgBase64"] options:0]];
            completionHandler(image, codeKey, nil);
        }
    }];
}


@end
