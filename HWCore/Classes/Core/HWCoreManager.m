//
//  HWCore.m
//  HWCore
//
//  Created by 胡伟 on 2019/1/14.
//

#import "HWCoreManager.h"
#import "FCFileManager.h"
#import "HWCoreAPI.h"


@implementation HWCoreManager

+ (instancetype)sharedCore {
    static HWCoreManager *core;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        core = [[HWCoreManager alloc] init];
    });
    return core;
}

- (BOOL)debug {
    #ifdef DEBUG
        //do sth.
        return YES;
    #else
        //do sth.
        return NO;
    #endif
}

- (HWModuleHelper *)moduleHelper {
    return [[HWModuleHelper alloc] init];
}

- (HWCoreAPI *)coreApi {
    return [HWCoreAPI sharedAPI];
}

@end
