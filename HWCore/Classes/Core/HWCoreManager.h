//
//  HWCore.h
//  HWCore
//
//  Created by 胡伟 on 2019/1/14.
//

#import <Foundation/Foundation.h>
#import "HWModuleHelper.h"
#import "HWCoreAPI.h"

NS_ASSUME_NONNULL_BEGIN

@interface HWCoreManager : NSObject

/**
 当前模式是否为调试模式
 */
@property (assign, nonatomic, readonly) BOOL debug;

@property (strong, nonatomic) HWModuleHelper *moduleHelper;

@property (strong, nonatomic) HWCoreAPI *coreApi;

+ (instancetype)sharedCore;

@end

NS_ASSUME_NONNULL_END
